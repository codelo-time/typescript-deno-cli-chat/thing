import color from 'https://deno.land/x/color/index.ts';

const writeToCLI = async (message: string): Promise<void> => {
	const buf = new TextEncoder().encode(message);
	await Deno.stdout.write(buf);
}

const takeFromCLI = async (): Promise<string> => {
	const buf = new Uint8Array(1024);
	const line = <number>await Deno.stdin.read(buf);
	return new TextDecoder().decode(buf.subarray(0, line)).trim();
}

export const nuPrompt = async (message: string): Promise<string> => {
	await writeToCLI(color.green.text(message) + color.white.text(' '));
	return await takeFromCLI();
}
