import { nuPrompt } from './nuPrompt.ts';
import ky from 'https://unpkg.com/ky/index.js';
import { User } from './interfaces.ts';

const userName: string = await nuPrompt("Enter user name:");
console.log(`Welcome, ${userName}`);

const resp: any = await ky.get(`/login/${userName}`).json();
if (resp.error) throw new Error('Unable to login');
const currentUser: User = <User>resp;

main: while (true) {
	const userCommand = await nuPrompt(`$${userName}>`);
	console.log('got', userCommand);

	switch (userCommand.trim()) {
		case '/exit': {
			const resp = await ky.get(`/logout/${currentUser.name}/${currentUser.id}`).json();
			if (resp.ok) break main;
			else throw new Error('Not a clean exit!');
		};

		default: {
			ky.post(`/post/${currentUser.name}/${currentUser.id}`, {json: {msg: userCommand.trim()}})
		}
	}
}