import { Application, Router, helpers } from 'https://deno.land/x/oak/mod.ts';
import { Context } from 'https://deno.land/x/oak/context.ts';
import { v4 } from 'https://deno.land/std/uuid/mod.ts';

import { User, Message } from './interfaces.ts';

const app: Application = new Application();

const users: Map<string, string> = new Map();

const loginUser = (name: string): boolean => {
	if (!users.has(name)) {
		users.set(name, v4.generate());
		return true;
	}

	return false;
}

const logoutUser = (name: string, uid: string): boolean => {
	if (users.has(name) && users.get(name) === uid) {
		users.delete(name);
		return true;
	}

	return false;
}

const messages: Set<Message> = new Set();


const router: Router = new Router();
router.get('/ping', async (ctx: Context) => {
	ctx.response.body = 'pong';
})

router.get('/login/:name', async (ctx: Context) => {
	const params: any = helpers.getQuery(ctx, { mergeParams: true });

	if (loginUser(params.name)) {
		ctx.response.body = JSON.stringify({
			name: `${params.name}`,
			id: users.get(params.name)
		});
	} else {
		ctx.response.body = JSON.stringify({
			error: 'User exists'
		});
	}
});

router.get('/logout/:name/:uid', async (ctx: Context) => {
	const params: any = helpers.getQuery(ctx, { mergeParams: true });

	if (logoutUser(params.name, params.uid)) {
		ctx.response.body = JSON.stringify({
			ok: true
		});
	} else {
		ctx.response.body = JSON.stringify({
			error: 'User is not logged in.'
		});
	}
})

router.post('/post/:name/:uid', async (ctx: Context) => {
	const params: any = helpers.getQuery(ctx, { mergeParams: true });
	const msg: {msg: string} = JSON.stringify(ctx.request.body);

	console.log(msg)

	// if (logoutUser(params.name, params.uid)) {
	// 	ctx.response.body = JSON.stringify({
	// 		ok: true
	// 	});
	// } else {
	// 	ctx.response.body = JSON.stringify({
	// 		error: 'User is not logged in.'
	// 	});
	// }
})


app.use(router.routes());

app.listen('localhost:8080');