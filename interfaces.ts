export interface User {
	name: string;
	id: string;
}

export interface Message {
	user: User;
	message: string;
}